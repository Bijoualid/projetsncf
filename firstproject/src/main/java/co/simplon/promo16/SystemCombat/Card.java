package co.simplon.promo16.SystemCombat;

import java.util.ArrayList;
import java.util.List;

public class Card {
    private String name;
    private int atk;
    private int shield;
    private int buffDebuff;
    private List<Object> boss1cardList = new ArrayList<>();
    // private List<Object> boss2cardList = new ArrayList<>();
    // private List<Object> boss3cardList = new ArrayList<>();
    private List<Object> hero1cardList = new ArrayList<>();

    public Card(String name, int atk, int shield, int buffDebuff) {
        this.name = name;
        this.atk = atk;
        this.shield = shield;
        this.buffDebuff = buffDebuff;
    }

}
