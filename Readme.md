# Sncf

Sncf est un jeu de cartes de type deck-building. Au début d'une partie, le joueur dispose d'un ensemble de points de vie, d'un score de bouclier et d'un jeu de cartes initial avec des cartes d'attaque et de défense de base. Le but est de vaincre un boss à la fin d'un niveau.
Le combat se joue à tour de rôle. A chaque tour, le joueur peut jouer une de ses cartes.
Les cartes varient selon le personnage mais consistent généralement en des cartes d'attaque pour endommager les adversaires, des cartes de bouclier pour ajouter un bouclier, des cartes d'amélioration/d'affaiblissements des adversaires.
